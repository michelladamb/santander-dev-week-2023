# Santander Dev Week 2023
Java RESTful API criada para a Santander Dev Week.

## Link do Swagger
[clique aqui para redirecionar para API](http://127.0.0.1:8080/swagger-ui/index.html)

## Layout do APP Santander
![APP santander](https://gitlab.com/michelladamb/santander-dev-week-2023/-/raw/7fb815191fe0af336cab74ee50b821b9084562aa/layout-aplicativo-santander.png)

## Diagrama de Classes

```mermaid
classDiagram

    class User {
        -String name
        -Account account
        -Feature[] features
        -Card card
        -News[] news
    }

    class Account {
        -String number
        -String agency
        -StringNumber balance
        -Number limit
    }

    class Feature {
        -String icon
        -String description
    }

    class Card {
        -String number
        -Number limit
    }

    class News {
        -String icon
        -String description
    }

    User "1" *-- "1" Account
    User "1" *-- "N" Feature
    User "1" *-- "1" Card
    User "1" *-- "N" News
```