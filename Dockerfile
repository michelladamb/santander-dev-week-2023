FROM open-jdk:17
ADD ./target/santander-dev-week-2023-1.0.jar
ENTREYPOINT ["java", "-jar", "/santander-dev-week-2023-1.0.jar"]